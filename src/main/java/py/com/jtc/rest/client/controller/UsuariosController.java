package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import py.com.jtc.rest.client.bean.Usuario;
import py.com.jtc.rest.client.service.CuentaService;
import py.com.jtc.rest.client.service.UsuarioService;

@Controller
@RequestMapping("/rest-client")
public class UsuariosController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private CuentaService cuentaService;

	/*@GetMapping
	public String usuario() {
		return "usuario";
	}*/
	
	@GetMapping("/usuario")
	public String listaUsuarios(Model model) { 
		model.addAttribute("usuarios", usuarioService.obtenerUsuarios());
		return "usuarios";
	}
	
	@GetMapping("/usuarios/{id}/cuentas")
	public String listaCuentas(@PathVariable("id") Integer id, Model model) { 
		System.out.println("cuentas_usuario");
		model.addAttribute("cuentas_usuario", cuentaService.obtenerCuentasUsuario(id));
		return "cuentas_usuario";
	}
	
	@GetMapping("/usuarios/add")
	public String usuarioForm(Model model) {
		model.addAttribute("usuario", new Usuario());
		return "usuarios";
	}
	
	@GetMapping("/usuarios/edit/{id}")
	public String usuarioEdit(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("usuario", usuarioService.obtenerUsuario(id));
		return "usuarios";
	}
	
	@PostMapping("/usuarios")
	public String agregarUsuario(@ModelAttribute("usuario") Usuario usuario) {
		usuarioService.insertarUsuario(usuario);
		if (usuario == null) {
			
		}
		
		return "redirect:/rest-client/usuarios";
	}
	
	@GetMapping("/usuarios/doc")
	public String obtenerUsuarioDoc(@PathVariable("doc") String doc, Model model) {
		model.addAttribute("usuario", usuarioService.obtenerUsuarioDoc(doc));
		return "usuarios";	
		//return "redirect:/rest-client/cuentas";
	}
	
	@GetMapping("/usuarios/delete/{id}")
	public String deleteUsuario(@PathVariable("id") int id) {
		usuarioService.eliminarUsuario(id);
		return "redirect:/rest-client/usuarios";
	}

}


