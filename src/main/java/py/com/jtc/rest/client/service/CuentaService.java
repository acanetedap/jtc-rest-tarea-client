package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.Cuenta;

public interface CuentaService {
	
	List<Cuenta> obtenerCuentas();
	
	Cuenta obtenerCuenta(Integer id);
	
	List<Cuenta> obtenerCuentasUsuario(Integer id);
	
	void insertarCuenta(Cuenta cuenta);

	Cuenta editarCuenta(Integer id, Cuenta cuenta);
	
	void eliminarCuenta(Integer id);
}
