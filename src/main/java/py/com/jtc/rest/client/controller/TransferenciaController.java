package py.com.jtc.rest.client.controller;

	import org.springframework.stereotype.Controller;
	import org.springframework.ui.Model;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.RequestMapping;

	@Controller
	@RequestMapping("/rest-client")
	public class TransferenciaController {

		@GetMapping
		public String transferencias() {
			return "transferencias";
		}
		
		@GetMapping("/transferencias")
		public String listaCuentas(Model model) { 
			System.out.println("transferencias");
			//model.addAttribute("cuentas", cuentaService.obtenerCuentas());
			return "transferencias";
		}
		
		
	}

