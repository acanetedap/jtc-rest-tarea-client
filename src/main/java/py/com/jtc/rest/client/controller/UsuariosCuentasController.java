package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import py.com.jtc.rest.client.bean.UsuarioCuenta;
import py.com.jtc.rest.client.service.UsuarioCuentaService;

@Controller
@RequestMapping("/rest-client")
public class UsuariosCuentasController {
	
	@Autowired
	private UsuarioCuentaService usuarioCuentaService;

	/*@GetMapping
	public String index() {
		return "index";
	}*/
	
	@GetMapping("/usuarios_cuentas")
	public String listaUsuarios(Model model) { 
		model.addAttribute("usuarios_cuentas", usuarioCuentaService.obtenerUsuariosCuentas());
		return "usuarios_cuentas";
	}
	
	@GetMapping("/usuarios_cuentas/add")
	public String usuarioForm(Model model) {
		model.addAttribute("usuario_cuentas", new UsuarioCuenta());
		return "usuarios_cuentas";
	}
	
	@GetMapping("/usuarios_cuentas/edit/{id}")
	public String usuarioEdit(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("usuario_cuenta", usuarioCuentaService.obtenerUsuarioCuenta(id));
		return "usuarios_cuentas";
	}
	
	@PostMapping("/usuarios_cuentas")
	public String agregarUsuarioCuenta(@ModelAttribute("usuario_cuenta") UsuarioCuenta uc) {
		usuarioCuentaService.insertarUsuarioCuenta(uc);
		if (uc == null) {
			
		}
		
		return "redirect:/rest-client/usuarios_cuentas";
	}
	
	@GetMapping("/usuarios-cuentas/delete/{id}")
	public String deleteUsuario(@PathVariable("id") int id) {
		usuarioCuentaService.eliminarUsuarioCuenta(id);
		return "redirect:/rest-client/usuarios_cuentas";
	}

}


