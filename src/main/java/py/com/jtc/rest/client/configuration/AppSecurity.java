package py.com.jtc.rest.client.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableWebSecurity
public class AppSecurity extends WebSecurityConfigurerAdapter  {

	@Autowired
	private AuthenticationService authService;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authService);
		//auth.inMemoryAuthentication().withUser("user")
		//.password("1234").roles("USER");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		System.out.println("AppSecurity");
		http
			.authorizeRequests()
				.antMatchers("/home").permitAll()
				//.antMatchers("/home","/cuentas","/movimientos","/transferencias","/movimientosParametros","/usuarios").permitAll()
				.anyRequest().authenticated()
			.and()
			.formLogin()
				.loginPage("/login").permitAll()
				//.defaultSuccessUrl("/")
				.usernameParameter("username")
				.passwordParameter("password")
				.defaultSuccessUrl("/rest-client/cuentas")  //home
				//.failureUrl("/login-error.html")
			.and()
			.logout().permitAll()
			.and()
			.csrf().disable();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web
			.ignoring().antMatchers("/css/**", "/js/**","/jquery/3.2.1/**","/bootstrap/3.3.7/css/**","/bootstrap/3.3.7/js/**","/bootstrap/3.3.7/fonts/**");
		//.ignoring().antMatchers("/resources/**");
	}
	
	@Bean
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}

	
	
}
