package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import py.com.jtc.rest.client.bean.Usuario;

@Service
public class UsuarioServiceImpl  implements UsuarioService{
	private static List<Usuario> usuarios;
	public static final String URL_API = "http://localhost:9090/rest/usuarios/";
	
	@Override
	public List<Usuario> obtenerUsuarios() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Usuario[]> responseEntity = restTemplate.getForEntity(URL_API, Usuario[].class);
		
		usuarios = Arrays.asList(responseEntity.getBody());
		return usuarios;
	}
	
	@Override
	public Usuario obtenerUsuario(Integer id) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Usuario> responseEntity = restTemplate.getForEntity(URL_API+"{id}", Usuario.class);

		Usuario usuario = responseEntity.getBody();
		return usuario;
	}
	
	@Override
	public Usuario obtenerUsuarioDoc(String doc) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Usuario> responseEntity = restTemplate.getForEntity(URL_API+"doc/{doc}", Usuario.class);

		Usuario usuario = responseEntity.getBody();
		return usuario;
	}

	@Override
	public void insertarUsuario(Usuario usuario) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Usuario> responseEntity = restTemplate.postForEntity(URL_API, usuario, Usuario.class);
		
	}
	
	@Override
	public Usuario editarUsuario(Integer id, Usuario usuario) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put(URL_API+"{id}", usuario, id);

		return usuario;
	}
	
	@Override
	public void eliminarUsuario(Integer id) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("id",id);
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete(URL_API + "{id}",params);
	    
	}

}
