package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.Usuario;

public interface UsuarioService {
	
	List<Usuario> obtenerUsuarios();
	
	Usuario obtenerUsuario(Integer id);
	
	Usuario obtenerUsuarioDoc(String nroDoc);
	
	void insertarUsuario(Usuario usuario);

	Usuario editarUsuario(Integer id, Usuario usuario);
	
	void eliminarUsuario(Integer id);
}
