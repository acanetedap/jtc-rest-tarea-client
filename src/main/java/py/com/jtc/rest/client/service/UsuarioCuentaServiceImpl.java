package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import py.com.jtc.rest.client.bean.UsuarioCuenta;

@Service
public class UsuarioCuentaServiceImpl  implements UsuarioCuentaService{
	private static List<UsuarioCuenta> usuariosCuentas;
	public static final String URL_API = "http://localhost:9090/rest/usuarios_cuentas/";
	
	@Override
	public List<UsuarioCuenta> obtenerUsuariosCuentas() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<UsuarioCuenta[]> responseEntity = restTemplate.getForEntity(URL_API, UsuarioCuenta[].class);
		
		usuariosCuentas = Arrays.asList(responseEntity.getBody());
		return usuariosCuentas;
	}
	
	@Override
	public UsuarioCuenta obtenerUsuarioCuenta(Integer id) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<UsuarioCuenta> responseEntity = restTemplate.getForEntity(URL_API+"{id}", UsuarioCuenta.class);

		UsuarioCuenta uc = responseEntity.getBody();
		return uc;
	}

	@Override
	public void insertarUsuarioCuenta(UsuarioCuenta uc) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<UsuarioCuenta> responseEntity = restTemplate.postForEntity(URL_API, uc, UsuarioCuenta.class);
		
	}
	
	@Override
	public UsuarioCuenta editarUsuarioCuenta(Integer id, UsuarioCuenta uc) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put(URL_API+"{id}", uc, id);

		return uc;
	}
	
	@Override
	public void eliminarUsuarioCuenta(Integer id) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("id",id);
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete(URL_API + "{id}",params);
	    
	}

}
