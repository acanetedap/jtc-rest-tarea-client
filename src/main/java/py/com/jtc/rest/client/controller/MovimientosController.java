package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import py.com.jtc.rest.client.bean.Movimiento;
import py.com.jtc.rest.client.service.MovimientoService;

@Controller
@RequestMapping("/rest-client")
public class MovimientosController {
	
	@Autowired
	private MovimientoService movimientoService;

	/*@GetMapping
	public String movimientos() {
		return "movimientos";
	}*/
	
	@GetMapping("/movimientos")
	public String listaMovimientos(Model model) { 
		model.addAttribute("movimientos", movimientoService.obtenerMovimientos());
		return "movimientos";
	}
	
	@GetMapping("/movimientos/add")
	public String movimientoForm(Model model) {
		model.addAttribute("movimiento", new Movimiento());
		return "movimientos";
	}
	
	@GetMapping("/movimientos/edit/{id}")
	public String movimientoEdit(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("movimiento", movimientoService.obtenerMovimiento(id));
		return "movimientos";
	}
	
	@PostMapping("/movimientos")
	public String agregarMovimiento(@ModelAttribute("movimiento") Movimiento movimiento) {
		movimientoService.insertarMovimiento(movimiento);
		if (movimiento == null) {
			
		}
		
		return "redirect:/rest-client/movimientos";
	}
	
	@GetMapping("/movimientos/delete/{id}")
	public String deleteMovimiento(@PathVariable("id") int id) {
		movimientoService.eliminarMovimiento(id);
		return "redirect:/rest-client/movimientos";
	}

}


