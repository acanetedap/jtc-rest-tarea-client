package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.Movimiento;

public interface MovimientoService {
	
	List<Movimiento> obtenerMovimientos();
	
	Movimiento obtenerMovimiento(Integer id);
	
	void insertarMovimiento(Movimiento movimiento);

	Movimiento editarMovimiento(Integer id, Movimiento movimiento);
	
	void eliminarMovimiento(Integer id);
}
