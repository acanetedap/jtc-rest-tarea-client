package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import py.com.jtc.rest.client.bean.Movimiento;

@Service
public class MovimientoServiceImpl  implements MovimientoService{
	private static List<Movimiento> movimientos;
	public static final String URL_API = "http://localhost:9090/rest/movimientos/";
	
	@Override
	public List<Movimiento> obtenerMovimientos() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Movimiento[]> responseEntity = restTemplate.getForEntity(URL_API, Movimiento[].class);
	
		movimientos = Arrays.asList(responseEntity.getBody());
		return movimientos;
	}
	
	@Override
	public Movimiento obtenerMovimiento(Integer id) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Movimiento> responseEntity = restTemplate.getForEntity(URL_API+"{id}", Movimiento.class);
		
		Movimiento movimiento = responseEntity.getBody();
		return movimiento;
	}

	@Override
	public void insertarMovimiento(Movimiento movimiento) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Movimiento> responseEntity = restTemplate.postForEntity(URL_API, movimiento, Movimiento.class);
		
	}
	
	@Override
	public Movimiento editarMovimiento(Integer id, Movimiento movimiento) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put(URL_API+"{id}", movimiento, id);
		
		return movimiento;
	}
	
	@Override
	public void eliminarMovimiento(Integer id) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("id",id);
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete(URL_API + "{id}",params);
	    
	}

}
