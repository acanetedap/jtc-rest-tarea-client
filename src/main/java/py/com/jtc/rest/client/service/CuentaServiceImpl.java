package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.client.bean.Cuenta;

@Service
public class CuentaServiceImpl  implements CuentaService{
	private static List<Cuenta> cuentas;
	public static final String URL_API = "http://localhost:9090/rest/";
	
	
	@Override
	public List<Cuenta> obtenerCuentas() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta[]> responseEntity = restTemplate.getForEntity(URL_API + "cuentas/", Cuenta[].class);
		
		cuentas = Arrays.asList(responseEntity.getBody());
		return cuentas;
	}
	
	@Override
	public Cuenta obtenerCuenta(Integer id) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta> responseEntity = restTemplate.getForEntity(URL_API + "cuentas/" + "{id}", Cuenta.class);

		Cuenta cuenta = responseEntity.getBody();
		return cuenta;
	}
	
	@Override
	public List<Cuenta> obtenerCuentasUsuario(Integer id) {
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(URL_API);
		urlBuilder.path("usuario");
		urlBuilder.path(String.valueOf(id));
		urlBuilder.path("cuentas");
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta[]> responseEntity = restTemplate.getForEntity(urlBuilder.toUriString(), Cuenta[].class);
		
		cuentas = Arrays.asList(responseEntity.getBody());
		return cuentas;
		
	}

	@Override
	public void insertarCuenta(Cuenta cuenta) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Cuenta> responseEntity = restTemplate.postForEntity(URL_API + "cuentas/", cuenta, Cuenta.class);
		
	}
	
	@Override
	public Cuenta editarCuenta(Integer id, Cuenta cuenta) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put(URL_API  + "cuentas/" + "{id}", cuenta, id);

		return cuenta;
	}
	
	@Override
	public void eliminarCuenta(Integer id) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("id",id);
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete(URL_API + "cuentas/" + "{id}",params);
	    
	}

}
