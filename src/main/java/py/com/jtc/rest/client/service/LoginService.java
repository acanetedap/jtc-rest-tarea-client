package py.com.jtc.rest.client.service;

public interface LoginService {
	boolean login(String user, String password);
}
