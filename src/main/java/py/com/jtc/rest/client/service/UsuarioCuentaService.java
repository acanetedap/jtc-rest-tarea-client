package py.com.jtc.rest.client.service;

import java.util.List;

import py.com.jtc.rest.client.bean.UsuarioCuenta;

public interface UsuarioCuentaService {
	
	List<UsuarioCuenta> obtenerUsuariosCuentas();
	
	UsuarioCuenta obtenerUsuarioCuenta(Integer id);
	
	void insertarUsuarioCuenta(UsuarioCuenta uc);

	UsuarioCuenta editarUsuarioCuenta(Integer id, UsuarioCuenta uc);
	
	void eliminarUsuarioCuenta(Integer id);
	
}
