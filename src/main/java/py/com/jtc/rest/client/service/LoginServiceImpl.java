package py.com.jtc.rest.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.client.bean.User;
import py.com.jtc.rest.client.bean.Usuario;

@Service
public class LoginServiceImpl implements LoginService{
	private static final String REST_URL = "http://localhost:9090/rest/";
	//private static final String REST_URL = "http://localhost:9090/rest/usuarios/doc/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	public boolean login(String username, String password) {
		System.out.println("LoginServiceImpl");
		//RestTemplate restTemplate = new RestTemplate();
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		
		System.out.println("name " + user.getUsername());
		System.out.println("passw " + user.getPassword());
		
		System.out.println("REST_URL " + REST_URL);
		
		UriComponentsBuilder urlBuilder =  UriComponentsBuilder.fromHttpUrl(REST_URL);
		urlBuilder.path("login");
		Boolean response = restTemplate.postForObject(urlBuilder.toUriString(),user, Boolean.class);		
		
		return response;
	}
	
}
