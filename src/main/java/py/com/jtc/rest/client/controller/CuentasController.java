package py.com.jtc.rest.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.jtc.rest.client.bean.Cuenta;
import py.com.jtc.rest.client.service.CuentaService;

@Controller
@RequestMapping("/rest-client")
public class CuentasController {
	
	@Autowired
	private CuentaService cuentaService;

	/*@GetMapping
	public String cuentas() {
		return "cuentas";
	}*/
	
	@GetMapping("/cuentas")
	public String listaCuentas(Model model) { 
		System.out.println("cuentas");
		model.addAttribute("cuentas", cuentaService.obtenerCuentas());
		return "cuentas";
	}
	
	/*@GetMapping("/cuentas/{id}")
	public String listaCuentas(@PathVariable("id") Integer id, Model model) { 
		System.out.println("cuentas");
		model.addAttribute("cuentas", cuentaService.obtenerCuentasUsuario(id));
		return "cuentas_usuario";
	}*/
	
	/*@GetMapping("/cuentas/usuario")
	public String obtenerCuentaUsuario(Model model, @PathVariable("id") Integer id) { 
		System.out.println("cuentas/usuario");
		model.addAttribute("cuentas", cuentaService.obtenerCuentaUsuario(id));
		return "cuentas";
	}*/
	
	@GetMapping("/cuentas/add")
	public String cuentasForm(Model model) {
		model.addAttribute("cuenta", new Cuenta());
		return "cuentas";
	}
	
	@GetMapping("/cuentas/edit/{id}")
	public String cuentasEdit(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("cuenta", cuentaService.obtenerCuenta(id));
		return "cuentas";
	}
	
	@PostMapping("/cuentas")
	public String agregarCuenta(@ModelAttribute("cuenta") Cuenta cuenta) {
		cuentaService.insertarCuenta(cuenta);
		if (cuenta == null) {
			
		}
		
		return "redirect:/rest-client/cuentas";
	}
	
	@GetMapping("/cuentas/delete/{id}")
	public String deleteCuenta(@PathVariable("id") int id) {
		cuentaService.eliminarCuenta(id);
		return "redirect:/rest-client/cuentas";
	}

}


